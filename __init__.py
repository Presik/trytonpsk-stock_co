# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import product
from . import position
from . import stock
from . import shipment
from . import inventory
from . import location


def register():
    Pool.register(
        product.Product,
        product.Template,
        product.AverageCost,
        position.ProductPosition,
        position.ProductTemplatePosition,
        stock.Move,
        stock.MoveByProductStart,
        stock.Lot,
        stock.WarehouseStockStart,
        stock.WarehouseStockDetailedStart,
        stock.PrintProductsStart,
        shipment.CreateInternalShipmentStart,
        shipment.InternalShipment,
        shipment.ShipmentIn,
        shipment.ShipmentOut,
        shipment.ShipmentDetailedStart,
        inventory.Inventory,
        inventory.InventoryLine,
        inventory.CreateInventoriesStart,
        stock.WarehouseKardexStockStart,
        stock.CreateOrderPointStart,
        product.ChangeUdmProductStart,
        location.ProductsByLocations,
        module='stock_co', type_='model')
    Pool.register(
        shipment.ShipmentDetailed,
        stock.WarehouseStockDetailed,
        stock.PrintMoveByProduct,
        stock.WarehouseKardexStock,
        shipment.CreateInternalShipment,
        stock.WarehouseStock,
        stock.PrintProducts,
        stock.CreateOrderPoint,
        shipment.ShipmentOutForceDraft,
        shipment.ShipmentInternalForceDraft,
        shipment.Assign,
        inventory.CreateInventories,
        shipment.ShipmentInForceDraft,
        shipment.ShipmentInReturnForceDraft,
        shipment.ShipmentInternalLoadStock,
        product.ChangeUdmProduct,
        module='stock_co', type_='wizard')
    Pool.register(
        stock.MoveByProduct,
        stock.WarehouseStockDetailedReport,
        stock.WarehouseKardexReport,
        shipment.ShipmentDetailedReport,
        stock.WarehouseReport,
        stock.PrintProductsReport,
        shipment.ShipmentInReturnReport,
        module='stock_co', type_='report')
