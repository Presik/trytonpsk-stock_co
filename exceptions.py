from trytond.exceptions import UserWarning

class DeleteWarning(UserWarning):
    pass